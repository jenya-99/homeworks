function start() {
  startBtn.style.visibility = 'hidden';
  pauseBtn.style.visibility = 'visible';
  pauseBtn.classList.add('btn-active');
  mainDiv.insertBefore(pauseBtn, startBtn);
  startTimer();
}
function  startTimer() {
  timerObj.startTime = new Date().getTime();//для автокоррекции
  timerObj.millisec += timerObj.step;
  if (timerObj.millisec < 100) {
    millisecId.textContent = '0' +  timerObj.millisec;
  }  
  if (timerObj.millisec >= 100 && timerObj.millisec < 1000) { 
    millisecId.textContent = timerObj.millisec;
  }
  if (timerObj.millisec == 1000) {
    millisecId.textContent = '000';
    timerObj.sec += 1;
    timerObj.millisec = 0;
  }  
  if (timerObj.sec < 10) {
    secId.textContent = '0' + timerObj.sec;
  }
  if (timerObj.sec >= 10 && timerObj.sec < 60) {
   secId.textContent = timerObj.sec;  
  }
  if (timerObj.sec == 60) {
    secId.textContent = '00';       
    timerObj.min += 1;
    timerObj.sec = 0;
  }    
  if (timerObj.min < 10) {
    minutesId.textContent = '0' + timerObj.min;
    }
  else minutesId.textContent = timerObj.min;
  //для автокоррекции:
  let	difference = new Date().getTime() - timerObj.startTime;
  //запуск setTimeout с подкорректированным delay: 
  timerObj.timerId = setTimeout(startTimer, timerObj.step - difference);      
}
function stop() {
  clearTimeout(timerObj.timerId);
  mainDiv.insertBefore(startBtn, pauseBtn);
  startBtn.style.visibility = 'visible';
  pauseBtn.style.visibility = 'hidden';
}
function clear() {
  stop();
  timerObj.millisec = 0;
  timerObj.sec = 0;
  timerObj.min = 0;
  timerObj.timerId = 0;
  timerObj.startTime = 0;
  millisecId.textContent = '000';
  secId.textContent = '00';
  minutesId.textContent = '00';  
}