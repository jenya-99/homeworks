// Фунция проверки на простоту числа n:
function checkPrim (n){
  if (n < 2) return false;// 0 и 1 - не являются простыми числами
  else { 
    for (let i = 2; i < n; i++){
      if (n % i == 0) return false;//если составное - вернет false
    }
    return true;//если 2 или другое простое число - вернет true 
  }
}