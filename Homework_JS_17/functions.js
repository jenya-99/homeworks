function removeElem () {
  $(this).remove();
}
function createInputs(event) {
  let input = $("<input/>")
    .addClass("inputText").attr("type", event.data.type)
    .attr("id", event.data.id).attr("placeholder", event.data.holder)
    .attr("autofocus", event.data.focus).attr('value', event.data.valueForBtn);
  $('body').append(input);
  if ($(input).attr('type') == 'button') {
    $(input).click(drawCircle);
  } 
}
function drawCircle() {
  let circle = $("<div/>").addClass("circle")
    .css({'width': sizeId.value, 'height': sizeId.value})    
    .css('backgroundColor', colorId.value)
    .css('borderRadius', sizeId.value / 2);
  $(inputBtnId).attr('disabled', 'disabled');
  $('body').append(circle); 
}