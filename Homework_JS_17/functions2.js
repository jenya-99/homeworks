function removeElem () {
  $(this).remove();
}
function createInputs(e) {
  let input = $("<input/>")
    .addClass("inputText").attr("type", e.data.type)
    .attr("id", e.data.id).attr("placeholder", e.data.holder)
    .attr("autofocus", e.data.focus).attr('value', e.data.valueForBtn);
  $('body').append(input);
  if ($(input).attr('type') == 'button') {
    $(input).click(drawCircle);
  }
}  
function creatColor() {
  let colorPalette = "0369cf".split('');
  let randomColor = "#";
  while (randomColor.length < 4) {
    randomColor += colorPalette.splice(Math.floor((Math.random() * colorPalette.length)), 1);
  }
  return randomColor;
}
function drawCircle() {  
  $('body').css('whiteSpace', 'nowrap');//чтобы круги не переносились
  for(let i = 0; i < 100; i++) {
  let circle = $("<div/>").addClass("circle")
    .css({'width': sizeId.value, 'height': sizeId.value})    
    .css('backgroundColor', creatColor())
    .css('borderRadius', sizeId.value / 2)
    .css('cursor', 'pointer');
    if (i % 10 == 0) {
      $('body').append("<br/>");//чтобы в ряду было 10 кругов
    }  
  $('body').append(circle); 
  $(inputBtnId).attr('disabled', 'disabled');//чтобы лишний раз не кликали ))
  }
  $('body').click(hideCircle);
}
function hideCircle(e) {
  if ($(e.target)[0].tagName != 'DIV') return;
  $(e.target).css('visibility', 'hidden');
}