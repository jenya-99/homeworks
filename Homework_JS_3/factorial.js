/*  Обычный синтаксис
function calcFact(n) {
  let result = 1;
  for (let i = 2; i <= n; i++) {
    result *= i;
  }
  return (n < 0 || parseInt(n) != n)? NaN: result;
}
*/

// Новый синтаксис:
let calcFact = (n) => {
  let result = 1;
  for (let i = 2; i <= n; i++) {
    result *= i;
  }
  return (n < 0 || parseInt(n) != n)? NaN: result; // Проверка на целое положит. число
}
let namber = prompt('Введите число');
alert(`${namber}! = ${calcFact(namber)}`);